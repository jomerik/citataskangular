import {
    ComponentFactoryResolver,
    ComponentRef,
    Directive,
    Input,
    OnChanges,
    OnInit,
    Type,
    ViewContainerRef
} from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';

import {FormInputComponent} from '../form-input/form-input.component';
import {FormCurrencyComponent} from '../form-currency/form-currency.component';
import {SectionComponent} from '../section/section.component';
import {FormDateComponent} from '../form-date/form-date.component';

import {Field} from '../models/field.interface';
import {FieldConfig} from '../models/field-config.interface';

const components: { [type: string]: Type<Field> } = {
    input: FormInputComponent,
    currency: FormCurrencyComponent,
    section: SectionComponent,
    date: FormDateComponent
};

@Directive({
    selector: '[dynamicField]'
})
export class DynamicFieldDirective implements Field, OnChanges, OnInit {
    @Input()
    inputs: FieldConfig;

    @Input()
    group: FormGroup;

    component: ComponentRef<Field>;

    constructor(private resolver: ComponentFactoryResolver,
                private container: ViewContainerRef) {
    }

    ngOnChanges() {
        if (this.component) {
            console.log(this.component);
            this.component.instance.inputs = this.inputs;
            this.component.instance.group = this.group;
        }
    }

    ngOnInit() {
        this.group = new FormGroup({
            input: new FormControl('this.inputs.type'),
            value: new FormControl(''),
            required: new FormControl(''),
        });

        const componentType = this.inputs.type;

        if (!components[componentType]) {
            const supportedTypes = Object.keys(components).join(', ');
            throw new Error(
                `Trying to use an unsupported type (${this.inputs.type}).
        Supported types: ${supportedTypes}`
            );
        }
        const component = this.resolver.resolveComponentFactory<Field>(components[componentType]);
        this.component = this.container.createComponent(component);
        this.component.instance.inputs = this.inputs;
        this.component.instance.group = this.group;
    }
}
