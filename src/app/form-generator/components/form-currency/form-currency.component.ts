import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';

import {Field} from '../models/field.interface';
import {FieldConfig} from '../models/field-config.interface';

@Component({
    selector: 'form-currency',
    styleUrls: ['form-currency.component.scss'],
    templateUrl: './form-currency.component.html'
})
export class FormCurrencyComponent implements Field, OnInit {
    inputs: FieldConfig;
    group: FormGroup;

    constructor(private fbuilder: FormBuilder) {
    }

    ngOnInit() {
        this.group = this.fbuilder.group({
            currency: this.inputs.value
        });
    }
}
