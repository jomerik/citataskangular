import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';

import {Field} from '../models/field.interface';
import {FieldConfig} from '../models/field-config.interface';

@Component({
    selector: 'form-date',
    styleUrls: ['form-date.component.scss'],
    templateUrl: 'form-date.component.html'
})
export class FormDateComponent implements Field, OnInit {
    inputs: FieldConfig;
    group: FormGroup;

    constructor(private fbuilder: FormBuilder) {
    }

    ngOnInit() {
        this.group = this.fbuilder.group({
            value: this.inputs.value
        });
    }
}
