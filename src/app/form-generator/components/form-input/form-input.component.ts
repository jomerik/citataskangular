import {Component, OnInit, ViewContainerRef, OnChanges} from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, NgForm } from '@angular/forms';

import { Field } from '../models/field.interface';
import { FieldConfig } from '../models/field-config.interface';

@Component({
  selector: 'form-input',
  styleUrls: ['form-input.component.scss'],
  templateUrl: 'form-input.component.html'
})
export class FormInputComponent implements Field, OnInit {
  inputs: FieldConfig;
  group: FormGroup;

  constructor(private fbuilder: FormBuilder) {
  }

  ngOnInit() {
    this.group = this.fbuilder.group({
      value: this.inputs.value
    });
  }
}
