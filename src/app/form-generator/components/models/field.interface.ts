import { FormGroup } from '@angular/forms';
import { FieldConfig } from './field-config.interface';

export interface Field {
  inputs: FieldConfig;
  group: FormGroup;
}
