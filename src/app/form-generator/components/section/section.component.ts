import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';

import {Field} from '../models/field.interface';
import {FieldConfig} from '../models/field-config.interface';

@Component({
    selector: 'section',
    styleUrls: ['section.component.scss'],
    templateUrl: './section.component.html'
})
export class SectionComponent implements Field, OnInit {
    inputs: FieldConfig;
    public group: FormGroup;

    constructor(private fb: FormBuilder) {
    }

    columns: {};

    ngOnInit() {
        this.columns = Array.apply(null, {length: this.inputs.columns}).map(Function.call, Number);
        this.group = this.fb.group({
            arrayOfinputs: this.fb.array([])
        });
    }

    public roundme(x) {
        return Math.ceil(x);
    }
}
