export class DataForm {
    id: string;
    name: string;
    type: string;
    items: object;
}
