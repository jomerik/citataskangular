import {NgModule, ModuleWithProviders, } from '@angular/core';
import {CommonModule} from '@angular/common';
import {JsonService} from './json.service';
import {DynamicFieldDirective} from './components/dynamic-field/dynamic-field.directive';
import {FormGeneratorComponent} from './form.generator';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SectionComponent} from './components/section/section.component';
import {FormInputComponent} from './components/form-input/form-input.component';
import {FormSelectComponent} from './components/form-select/form-select.component';
import {FormCurrencyComponent} from './components/form-currency/form-currency.component';
import {FormDateComponent} from './components/form-date/form-date.component';

@NgModule({
    imports: [CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule],
    declarations: [
        FormGeneratorComponent,
        DynamicFieldDirective,
        SectionComponent,
        FormInputComponent,
        FormSelectComponent,
        FormCurrencyComponent,
        FormDateComponent,
    ],
    entryComponents: [
        SectionComponent,
        FormInputComponent,
        FormCurrencyComponent,
        FormSelectComponent,
        FormDateComponent,
    ],
    exports: [FormGeneratorComponent]
})
export class FormGeneratorModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: FormGeneratorModule,
            providers: [JsonService]
        };
    }
}
