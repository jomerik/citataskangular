import {Component, OnInit, Input} from '@angular/core';
import {JsonService} from './json.service';
import {FormGroup, FormBuilder, NgForm} from '@angular/forms';
import {DataForm} from './dataform';
import {FieldConfig} from './components/models/field-config.interface';

@Component({
    selector: 'form-generator',
    templateUrl: 'form.generator.html',
    styleUrls: ['form.generator.css'],
    providers: [JsonService]
})
export class FormGeneratorComponent implements OnInit {
    @Input() file: string;
    inputs: DataForm[] = [];
    config: FieldConfig[] = [];

    public form: FormGroup;

    constructor(private jsonService: JsonService, private fb: FormBuilder) {
    }

    onSubmit(myform: NgForm): void {
        console.log('form submitted');
    }

    ngOnInit(): void {
        this.jsonService.getInputs(this.file)
            .subscribe(inputs => {
                this.inputs = inputs;
                this.form = this.jsonService.toFormGroup(inputs);
            });
    }

    rebuildForm() {
        this.form.reset({});
    }

    revert() {
        this.rebuildForm();
    }
}
