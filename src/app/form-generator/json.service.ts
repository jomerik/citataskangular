import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {DataForm} from './dataform';
import {FormGroup, FormControl} from '@angular/forms';

@Injectable()
export class JsonService {
    constructor(private http: HttpClient) {
    }

    // here can be json file or http url
    private _url = 'assets/form-content.json';
    json: any;

    getInputs(file: string): Observable<DataForm[]> {
        return this.http.get(this._url).pipe(map(data => {
            const inputs: DataForm[] = [];

            this.json = data['widgets'].filter(function (item) {
                return item.type === 'form';
            });

            for (const ind in this.json) {

                const Form = this.json[ind];
                const input: DataForm = {id: Form.id, name: Form.name, type: Form.type, items: Form.items};
                inputs.push(input);
            }
            return inputs;
        }));

    }

    toFormGroup(inputs: DataForm[]): FormGroup {
        const group: any = {};

        inputs.forEach(input => {
            group[input.type + input.id] = new FormControl(input.name);
            group['items'] = new FormControl(input.items);
        });
        return new FormGroup(group);
    }
}
